package com.jobinlawrance.assignment

import android.app.Application
import androidx.room.Room
import com.jobinlawrance.assignment.data.db.AppDatabase

class AssignmentApplication : Application() {
    var dataBase: AppDatabase? = null

    override fun onCreate() {
        super.onCreate()
        dataBase = Room.databaseBuilder(this, AppDatabase::class.java, "assignment-db").build()
    }
}