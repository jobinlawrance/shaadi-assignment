package com.jobinlawrance.assignment.ui

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.button.MaterialButton
import com.jobinlawrance.assignment.R
import com.jobinlawrance.assignment.data.model.entities.UserEntity
import com.jobinlawrance.assignment.data.model.entities.UserStatus

class UserListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    val textView = itemView.findViewById<TextView>(R.id.tv_user_name)
    val imageView = itemView.findViewById<ImageView>(R.id.iv_user)
    val buttonLayout = itemView.findViewById<LinearLayout>(R.id.ll_button_layout)
    val statusText = itemView.findViewById<TextView>(R.id.tv_selection_text)
    val buttonAccept = itemView.findViewById<MaterialButton>(R.id.button_accept)
    val buttonReject = itemView.findViewById<MaterialButton>(R.id.button_reject)

    fun setUserListItem(userEntity: UserEntity, statusClickListener: (UserEntity, UserStatus) -> Unit) {
        Glide.with(itemView).load(userEntity.picture.large).apply(RequestOptions().circleCrop()).into(imageView)
        textView.text = userEntity.name.first

        if (userEntity.status == UserStatus.UNDECIDED) {
            buttonLayout.visibility = View.VISIBLE
            statusText.visibility = View.GONE
        } else {
            buttonLayout.visibility = View.GONE
            statusText.visibility = View.VISIBLE
            if (userEntity.status == UserStatus.ACCEPTED) {
                statusText.text = "You have accepted"
            } else {
                statusText.text = "You have rejected"
            }
        }

        //click listener
        buttonAccept.setOnClickListener {
            statusClickListener(userEntity, UserStatus.ACCEPTED)
        }

        buttonReject.setOnClickListener {
            statusClickListener(userEntity, UserStatus.REJECTED)
        }
    }
}