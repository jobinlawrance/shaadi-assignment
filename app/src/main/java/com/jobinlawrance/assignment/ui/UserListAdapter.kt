package com.jobinlawrance.assignment.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.jobinlawrance.assignment.R
import com.jobinlawrance.assignment.data.model.entities.UserEntity
import com.jobinlawrance.assignment.data.model.entities.UserStatus

class UserListAdapter(private val statusClickListener: (UserEntity, UserStatus) -> Unit) :
    ListAdapter<UserEntity, UserListViewHolder>(UserDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(R.layout.holder_user_list_item, parent, false)
        return UserListViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: UserListViewHolder, position: Int) {
        holder.setUserListItem(getItem(position), statusClickListener)
    }

    fun updateUserList(newList: List<UserEntity>) {
        submitList(newList)
    }
}

class UserDiffCallback : DiffUtil.ItemCallback<UserEntity>() {
    override fun areItemsTheSame(oldItem: UserEntity, newItem: UserEntity): Boolean {
        return oldItem.login.uuid == newItem.login.uuid
    }

    override fun areContentsTheSame(oldItem: UserEntity, newItem: UserEntity): Boolean {
        return oldItem.status.code == newItem.status.code
    }
}