package com.jobinlawrance.assignment.ui

import android.util.Log
import androidx.lifecycle.*
import com.jobinlawrance.assignment.data.api.ApiService
import com.jobinlawrance.assignment.data.db.UserDao
import com.jobinlawrance.assignment.data.model.entities.*
import com.jobinlawrance.assignment.data.model.remote.RandomApiResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import java.lang.IllegalStateException

class UserViewModel(private val userDao: UserDao, private val apiService: ApiService) :
    ViewModel() {

    private val _liveData: MutableLiveData<List<UserEntity>> = MutableLiveData()
    val liveData: LiveData<List<UserEntity>>
        get() = _liveData

    private val disposables = CompositeDisposable()
    private var didStart = false

    fun start() {
        if (didStart) return

        didStart = !didStart

        disposables += userDao.getAllUsersObservable()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ listOfUserEntity ->
                _liveData.postValue(listOfUserEntity)
            }, {
                Log.e("###Dao", "Observable error", it)
            })

        disposables += apiService.getUsers()
            .subscribeOn(Schedulers.io())
            .subscribe({ apiResponse: RandomApiResponse ->
                mapAndInsertUserInDb(apiResponse, userDao)
            }, { error: Throwable? ->
                Log.e("###Api", "Failed", error)
            })
    }

    fun updateUserStatus(userEntity: UserEntity, userStatus: UserStatus) {
        disposables += userDao.updateUserStatus(userEntity.login.uuid, userStatus)
            .subscribeOn(Schedulers.io())
            .subscribe()
    }

    fun mapAndInsertUserInDb(
        randomApiResponse: RandomApiResponse,
        userDao: UserDao
    ) { //TODO - create a mapper class
        val userEntities: List<UserEntity> = randomApiResponse.results.map { user ->
            UserEntity(
                user.cell,
                Dob(user.dob.age, user.dob.date),
                "", "",
                Login(
                    user.login.md5,
                    user.login.password,
                    user.login.salt,
                    user.login.sha1,
                    user.login.sha256,
                    user.login.username,
                    user.login.uuid
                ),
                Name(user.name.first, user.name.last, user.name.title),
                "",
                user.phone,
                Picture(user.picture.large, user.picture.medium, user.picture.thumbnail),
                UserStatus.UNDECIDED
            )
        }

        userDao.insertAll(userEntities)
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}

@Suppress("UNCHECKED_CAST")
class UserViewModelFactory(val userDao: UserDao, val apiService: ApiService) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(UserViewModel::class.java)) {
            UserViewModel(userDao, apiService) as T
        } else {
            throw IllegalStateException("ViewModel not Found")
        }
    }
}