package com.jobinlawrance.assignment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jobinlawrance.assignment.data.model.entities.UserEntity
import com.jobinlawrance.assignment.data.model.entities.UserStatus
import com.jobinlawrance.assignment.ui.UserListAdapter
import com.jobinlawrance.assignment.ui.UserViewModel
import com.jobinlawrance.assignment.ui.UserViewModelFactory
import io.reactivex.disposables.CompositeDisposable

class MainActivity : AppCompatActivity() {

    var disposables: CompositeDisposable = CompositeDisposable()
    lateinit var recyclerView: RecyclerView
    lateinit var adapter: UserListAdapter
    lateinit var statusClickListener: (UserEntity, UserStatus) -> Unit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val apiService = AppModule.getApiService()

        val application: AssignmentApplication = application as AssignmentApplication
        val userDao = application.dataBase!!.userDao()

        val userViewModel = ViewModelProvider(this, UserViewModelFactory(userDao, apiService))
            .get(UserViewModel::class.java)

        statusClickListener = { userEntity, userStatus ->
            userViewModel.updateUserStatus(userEntity, userStatus)
        }

        recyclerView = findViewById(R.id.recyclerView)
        adapter = UserListAdapter(statusClickListener)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        userViewModel.liveData.observe(this, Observer { userEntityList ->
            adapter.updateUserList(userEntityList)
        })

        userViewModel.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.dispose()
    }
}
