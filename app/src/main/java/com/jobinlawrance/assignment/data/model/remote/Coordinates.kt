package com.jobinlawrance.assignment.data.model.remote

data class Coordinates(
    val latitude: String,
    val longitude: String
)