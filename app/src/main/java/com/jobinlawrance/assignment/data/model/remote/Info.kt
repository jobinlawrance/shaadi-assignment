package com.jobinlawrance.assignment.data.model.remote

data class Info(
    val page: Int,
    val results: Int,
    val seed: String,
    val version: String
)