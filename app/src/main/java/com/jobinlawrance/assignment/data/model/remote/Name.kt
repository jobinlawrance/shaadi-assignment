package com.jobinlawrance.assignment.data.model.remote

data class Name(
    val first: String,
    val last: String,
    val title: String
)