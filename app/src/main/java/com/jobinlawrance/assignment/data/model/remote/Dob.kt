package com.jobinlawrance.assignment.data.model.remote

data class Dob(
    val age: Int,
    val date: String
)