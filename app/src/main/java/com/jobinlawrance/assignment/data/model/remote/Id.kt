package com.jobinlawrance.assignment.data.model.remote

data class Id(
    val name: String,
    val value: String?
)