package com.jobinlawrance.assignment.data.model.remote

data class Street(
    val name: String,
    val number: Int
)