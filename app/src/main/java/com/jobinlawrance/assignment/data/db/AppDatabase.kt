package com.jobinlawrance.assignment.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.jobinlawrance.assignment.data.model.entities.UserEntity
import com.jobinlawrance.assignment.data.model.entities.UserStatusTypeConverter

@Database(entities = [UserEntity::class],version = 1)
@TypeConverters(UserStatusTypeConverter::class)
abstract class AppDatabase: RoomDatabase() {
    abstract fun userDao(): UserDao
}