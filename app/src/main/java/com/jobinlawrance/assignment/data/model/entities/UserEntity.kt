package com.jobinlawrance.assignment.data.model.entities

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.TypeConverter

@Entity(tableName = "user", primaryKeys = ["login_uuid"])
data class UserEntity(
    var cell: String,
    @Embedded(prefix = "dob_")
    var dob: Dob,
    var email: String,
    var gender: String,
    @Embedded(prefix = "login_")
    var login: Login,
    @Embedded(prefix = "name_")
    var name: Name,
    var nat: String,
    var phone: String,
    @Embedded(prefix = "picture_")
    var picture: Picture,
    var status: UserStatus = UserStatus.UNDECIDED
)

data class Login(
    var md5: String = "",
    var password: String = "",
    var salt: String = "",
    var sha1: String = "",
    var sha256: String = "",
    var username: String = "",
    var uuid: String = ""
)

data class Dob(
    var age: Int = 1,
    var date: String = ""
)

data class Name(
    val first: String,
    val last: String,
    val title: String
)

data class Picture(
    val large: String,
    val medium: String,
    val thumbnail: String
)

enum class UserStatus(val code: Int) {
    UNDECIDED(0),
    ACCEPTED(1),
    REJECTED(2)
}

class UserStatusTypeConverter {
    @TypeConverter
    fun toStatus(code: Int): UserStatus {
        return when(code) {
            0 -> UserStatus.UNDECIDED
            1 -> UserStatus.ACCEPTED
            2 -> UserStatus.REJECTED
            else -> UserStatus.UNDECIDED
        }
    }

    @TypeConverter
    fun toInteger(status: UserStatus): Int {
        return status.code
    }
}