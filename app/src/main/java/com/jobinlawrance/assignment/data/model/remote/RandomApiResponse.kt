package com.jobinlawrance.assignment.data.model.remote

data class RandomApiResponse(
    val info: Info,
    val results: List<User>
)