package com.jobinlawrance.assignment.data.api

import com.jobinlawrance.assignment.data.model.remote.RandomApiResponse
import io.reactivex.Observable
import retrofit2.http.GET

interface ApiService {
    @GET("api/?results=20")
    fun getUsers(): Observable<RandomApiResponse>
}