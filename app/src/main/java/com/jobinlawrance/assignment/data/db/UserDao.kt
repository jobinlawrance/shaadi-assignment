package com.jobinlawrance.assignment.data.db

import androidx.room.*
import com.jobinlawrance.assignment.data.model.entities.UserEntity
import com.jobinlawrance.assignment.data.model.entities.UserStatus
import com.jobinlawrance.assignment.data.model.entities.UserStatusTypeConverter
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable

@Dao
interface UserDao {
    @Query("SELECT * FROM user")
    fun getAll(): List<UserEntity>

    @Query("SELECT * FROM user")
    fun getAllUsersObservable(): Observable<List<UserEntity>>

    @Insert
    fun insertAll(vararg users: UserEntity)

    @Insert
    fun insertAll(users: List<UserEntity>)

    @Update
    fun update(vararg users: UserEntity)

    @Query("UPDATE user SET status = :status WHERE login_uuid = :loginUuid")
    fun updateUserStatus(loginUuid: String, status: UserStatus): Maybe<Int>
}