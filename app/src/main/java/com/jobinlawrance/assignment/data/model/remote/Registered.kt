package com.jobinlawrance.assignment.data.model.remote

data class Registered(
    val age: Int,
    val date: String
)