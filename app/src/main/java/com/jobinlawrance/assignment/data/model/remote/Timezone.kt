package com.jobinlawrance.assignment.data.model.remote

data class Timezone(
    val description: String,
    val offset: String
)