package com.jobinlawrance.assignment

import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.jobinlawrance.assignment.data.db.AppDatabase
import com.jobinlawrance.assignment.data.db.UserDao
import com.jobinlawrance.assignment.data.model.entities.*
import com.jobinlawrance.assignment.data.model.remote.User
import org.junit.*
import org.junit.rules.TestRule
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UserDaoTest {

    @Rule
    @JvmField
    val rule: TestRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var userDao: UserDao

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        try {
            database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java)
                .allowMainThreadQueries().build()
        } catch (e: Exception) {
            Log.e("test", e.message)
        }
        userDao = database.userDao()
    }

    @Test
    fun testAddingAndRetrievingData() {
        val preInsertUsers = userDao.getAll()

        val USER = UserEntity(
            cell = "",
            dob = Dob(),
            name = Name("J", "L", "Mr"),
            email = "",
            gender = "",
            login = Login(uuid = "asdf=asfn"),
            nat = "",
            phone = "",
            picture = Picture("", "", "")
        )

        userDao.insertAll(USER)

        val postInsertUser = userDao.getAll()
        val sizeDiff = postInsertUser.size - preInsertUsers.size
        Assert.assertEquals(1, sizeDiff)
        val retrievedUser = postInsertUser.last()
        Assert.assertEquals(USER.login.uuid, retrievedUser.login.uuid)
    }

    @Test
    fun testUserStatusTypeConversion() {
        val USER = UserEntity(
            cell = "",
            dob = Dob(),
            name = Name("J", "L", "Mr"),
            email = "",
            gender = "",
            login = Login(uuid = "asdf=asfn"),
            nat = "",
            phone = "",
            picture = Picture("", "", ""),
            status = UserStatus.UNDECIDED
        )

        userDao.insertAll(USER)

        val userRetrieved = userDao.getAll().last()

        Assert.assertEquals(userRetrieved.status, UserStatus.UNDECIDED)
    }

    @After
    fun tearDown() {
        database.close()
    }
}